#include <iostream>
#include <string>
#include <cstdlib>
#include <new>
#include "Vector.h"

using namespace std;


Vector::Vector(int n = 10)
{
	_size = n;
	_capacity = 2*n;
	_resizeFactor = 2*n;
	try
	{
		_begin = new int[2 * n];
	}
	catch (std::bad_alloc& ba)
	{
		std::cerr << "bad_alloc caught: " << ba.what() << '\n';
		exit(EXIT_FAILURE);
	}
	
}

Vector::Vector(const Vector& v)
{
	_capacity = v._capacity;
	_size = v._size;
	_resizeFactor = v._resizeFactor;
	delete[] _begin;
	_begin = new int[_capacity];
	for (int i = 0; i < v._size; i++)
	{
		_begin[i] = v._begin[i];
	}
}

	

int Vector::size()
{
	return(_size);
}
	
int Vector::capacity()
{
	return(_capacity);
}

bool Vector::empty()
{
	return(_size == 0);
}

void Vector::assign(int val)
{
	int i;
	for (i = 0; i < _size; i++)
	{
		_begin[i] = val;
	}
}
	
void Vector::clear()
{
	delete[] _begin;
	_size = 0;
	_capacity = 0;
}

void Vector::push_back(const int& val)
{
	if (_size < _capacity)
	{
		_begin[_size] = val;
		_size++;
	}
	else
	{
		reserve(_capacity + 1);
		_begin[_size] = val;
		_size++;
	}
}

void Vector::pop_back()
{
	_size--;
}

void Vector::reserve(int n)
{
	int *arr;
	int i;
	if (n > _capacity)
	{
		try
		{
			arr = new int[n + _resizeFactor];
		}
		catch (std::bad_alloc& ba)
		{
			std::cerr << "bad_alloc caught: " << ba.what() << '\n';
			exit(EXIT_FAILURE);
		}
		for (i = 0; i < _size; i++)
		{
			arr[i] = _begin[i];
		}
		delete (_begin);

		try
		{
			_begin = new int[_capacity + _resizeFactor];
		}
		catch (std::bad_alloc& ba)
		{
			std::cerr << "bad_alloc caught: " << ba.what() << '\n';
			exit(EXIT_FAILURE);
		}

		for (i = 0; i < _size; i++)
		{
			_begin[i] = arr[i];
		}
		delete (arr);
		_capacity = _resizeFactor + n;
	}
}

void Vector::resize(int n)
{

	if (n < _size)
	{
		_size = n;
	}
	else if (n > _size && n < _capacity)
	{
		_size = n;
	}
	else if (n > _capacity)
	{
		reserve(n);
		_size = n;		
	}

}

void Vector::resize(int n, const int& val)
{
	int i;
	if (n < _size)
	{
		_size = n;
	}
	else if (n>_size && n < _capacity)
	{
		for (i = _size; i < n; i++)
		{
			_begin[i] = val;
		}
		_size = n;
	}
	else if (n >= _capacity)
	{
		reserve(n);
		for (i = _size; i < n; i++)
		{
			_begin[i] = val;
		}
		_size = n;
	}
}

Vector& Vector::operator=(const Vector& v)
{
	_capacity = v._capacity;
	_size = v._size;
	_resizeFactor = v._resizeFactor;
	delete[] _begin;
	_begin = new int[_capacity];
	for (int i = 0; i < v._size; i++)
	{
		_begin[i] = v._begin[i];
	}
	return(*this);
}

int& Vector::operator[](int i)
{
	if (i < 0 || i >= _size)
		throw std::out_of_range("out of range");
	int a = _begin[i];
	int &b = a;
	return(b);
}

Vector::~Vector()
{
	delete[] _begin;
}