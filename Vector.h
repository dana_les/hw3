#ifndef VECTOR_H
#define VECTOR_H

#include <iostream>
#include <string>
#include <cstdlib>

class Vector
{
	public:
		Vector(int n);
		Vector(const Vector& v);
		int size();			//return the size
		int capacity();		//return the capacity
		bool empty();			//return if the array is empty
		void assign(int val);			//the function write the value in all the possible places
		void clear();	//
		void push_back(const int& val);
		void pop_back();
		void reserve(int n);
		void resize(int n);
		void resize(int n, const int& val);
		Vector& operator=(const Vector& v);
		int& operator[](int i);
		~Vector();

	private:
		int *_begin;
		int _size;
		int _capacity;
		int _resizeFactor;
};
#endif